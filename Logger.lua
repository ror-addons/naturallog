if not lnLogger then lnLogger = {} end

lnLogger = Frame:Subclass("lnLogger")

function lnLogger:New(limit, buffer)
	local l = setmetatable({}, self)
	
	l.buffer = buffer or 20
	l.limit = limit or 300
	l.activecount = 0
	l.totalcount = 0
	l.log = {}
	
	return( l )
end 

function lnLogger:AddEntry( event, scroll )
	if not self.limit then return end
	if event.text == "" then event.text = nil end
	
	self.totalcount = self.totalcount + 1
	self.activecount = self.activecount + 1
	self.log[self.totalcount] = event
	
	-- Remove excess logs unless we are scrolled to them
	if self.activecount > self.limit and self.activecount > self.buffer + scroll then
		self.activecount = self.activecount - 1
		self.log[self.totalcount - self.activecount] = nil
	end
	
	-- shift scroll if scrolled, and notiify via return if things changed
	if scroll > 0 then return false end
	return true
end

function lnLogger:GetEntry( offset )
	if not self.limit then return end
	return( self.log[self.totalcount - offset] )
end 


function lnLogger:Trim( scroll )
	if not self.limit then return end
	while self.activecount > self.limit and self.activecount > self.buffer + scroll do
		self.activecount = self.activecount - 1
		self.log[self.totalcount - self.activecount] = nil
	end
end 
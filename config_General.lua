-------------------------------------
--  Natural Log Config  --  version: 1.0.1
--  
--  Config panel for the bar in general
--
--     Written By NigelTufnel ( Adam.Dew@gmail.com )
-------------------------------------

if not lnConfig then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel, SetupPanel, RevertPanel, gAlpha, gOrient, gHeight, gWidth
local panel = {}

-- Our panel window
local window
local settingfont = "font_clear_small_bold"
panel.title = L"General Settings"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if window and window.name then return window end
    
    window = LibGUI("Blackframe")
    window:Resize(420, 430)
    
    local e
    
    -- Title label
    e = window("Label")
    e:Resize(480)
    e:Font("font_clear_large_bold")
    e:SetText(L"General Settings")
    e:AnchorTo(window, "top", "top", 0, 8)
    window.Title = e
    
	
	
    -- Bar label
    e = window("Label")
    e:Resize(90)
    e:AnchorTo(window, "topleft", "topleft", 30, 45)
    e:Font("font_clear_large_bold")
    e:Align("bottomleft")
	e:Color(255,255,255)
    e:SetText(L"Bar")
    window.BarLabel = e
	
	e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.BarLabel, "bottomright", "topright", -5, 5)
    e:SetText("Layout")
	e:Font(settingfont)
    window.Orientation = e
	
    e = window("smallCombobox")
    e:AnchorTo(window.Orientation, "right", "left", 5, 0)
	e:Add(L"Vertical")
	e:Add(L"Horizontal")
	e:SelectIndex(1)
    window.OrientationBox = e
    e = window("Label")
	
    e = window("TinyCombobox")
    e:AnchorTo(window.OrientationBox, "right", "left", 5, 0)
	e:Add(L"Up")
	e:Add(L"Down")
	e:SelectIndex(1)
    window.DirectionBox = e
	
	
    e = window("Label")
    e:Resize(60)
    e:Align("rightcenter")
    e:AnchorTo(window.Orientation, "bottomright", "topright", 0, 5)
    e:SetText("Height")
	e:Font(settingfont)
    window.Height = e
	
    e = window("Slider")
    e:AnchorTo(window.Height, "right", "left", 3, 0)
    e:SetRange(0,1)
    window.SliderHeight = e
	
    e = window("Textbox")
    e:Resize(70)
    e:AnchorTo(window.SliderHeight, "right", "left", 10, 0)
    e:SetText(255)
    window.EditHeight = e	
	
    e = window("Label")
    e:Resize(60)
    e:Align("rightcenter")
    e:AnchorTo(window.Height, "bottom", "top", 0, 5)
    e:SetText("Width")
	e:Font(settingfont)
    window.Width = e
	
    e = window("Slider")
    e:AnchorTo(window.Width, "right", "left", 3, 0)
    e:SetRange(0,1)
    window.SliderWidth = e
	
    e = window("Textbox")
    e:Resize(70)
    e:AnchorTo(window.SliderWidth, "right", "left", 10, 0)
    e:SetText(255)
    window.EditWidth = e
	
	
	
	
	
	-- ALHPA
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.Width, "bottomright", "topright", 0, 5)
    e:SetText("Opacity")
	e:Font(settingfont)
    window.LabelAlpha = e
	
    e = window("Slider")
    e:AnchorTo(window.LabelAlpha, "right", "left", 3, 0)
    e:SetRange(0,100)
    window.SliderAlpha = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderAlpha, "right", "left", 10, 0)
    e:SetText(100)
    window.EditAlpha = e
	
	
    e = window("Label")
    e:Resize(143)
    e:Align("rightcenter") 
    e:AnchorTo(window.LabelAlpha, "bottomright", "topright", 40, 5)
    e:SetText("Events Shown")
	e:Font(settingfont)
    window.NumLines = e
	
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.NumLines, "right", "left", 5, 0)
    e:SetText(30)
    window.EditNumLines = e
	

	
	
    -- Log Settings
    e = window("Label")
    e:Resize(90)
    e:AnchorTo(window.BarLabel, "bottom", "top", 0, 175)
    e:Font("font_clear_large_bold")
    e:Align("bottomleft")
	e:Color(255,255,255)
    e:SetText(L"Log")
    window.LogLabel = e
	
	--Aligns	
    e = window("Label")
    e:Resize(140)
    e:Align("rightcenter")
    e:AnchorTo(window.LogLabel, "bottomleft", "topleft", 10, 5)
    e:SetText("Allow icons on ")
	e:Font(settingfont)
    window.IconAlign = e
	
    e = window("smallCombobox")
    e:AnchorTo(window.IconAlign, "right", "left", 4, 0)
	e:Add(L"Left")
	e:Add(L"Right")
	e:Add(L"Either")
	e:SelectIndex(1)
    window.IconAlignBox = e
	
    e = window("Label")
    e:Resize(60)
    e:Align("leftcenter")
    e:AnchorTo(window.IconAlignBox, "right", "left", 4, 0)
    e:SetText("side")
	e:Font(settingfont)
    window.IconAlign2 = e
	
	
    e = window("Label")
    e:Resize(90)
    e:Align("rightcenter")
    e:AnchorTo(window.IconAlign, "bottomleft", "topleft", 0, 5)
    e:SetText("History")
	e:Font(settingfont)
    window.History = e
	
    e = window("Textbox")
    e:Resize(60)
    e:AnchorTo(window.History, "right", "left", 2, 0)
    window.HistoryBox = e
	
    
    -- Apply button
    e = window("Button")
    e:Resize(175)
    e:SetText(L"Apply")
    e:AnchorTo(window, "bottomleft", "bottomleft", 25, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    window.ButtonApply = e
    
    -- Revert button
    e = window("Button")
    e:Resize(175)
    e:SetText(L"Undo")
    e:AnchorTo(window, "bottomright", "bottomright", -25, -20)
    e.OnLButtonUp = function() SetupPanel() end
    window.ButtonRevert = e
    
	SetupPanel()

    return window
end

function ApplyPanel()
	if tonumber( window.HistoryBox:GetText() ) >= 0 then
		NaturalLog.Assign( "loglimit", tonumber( window.HistoryBox:GetText() ) )
	end
	if tonumber(window.EditAlpha:GetText()) >= 0 then
		NaturalLog.Assign( "alpha", tonumber( window.EditAlpha:GetText() ) / 100 )
	end
	if tonumber( window.EditNumLines:GetText() ) >= 0 then
		NaturalLog.Assign( "lines", tonumber( window.EditNumLines:GetText() ) )
	end
	
	NaturalLog.Assign( "size", window.SliderWidth:GetValue(), window.SliderHeight:GetValue() )
	
	if window.OrientationBox:SelectedIndex() == 1 then
		NaturalLog.Assign( "orient", false )
	else
		NaturalLog.Assign( "orient", true )
	end
	
	if window.DirectionBox:SelectedIndex() == 1 then
		NaturalLog.Assign( "direction", true )
	else
		NaturalLog.Assign( "direction", false )
	end
	
	NaturalLog.Set( "spacing", window.IconAlignBox:SelectedIndex() )
	
	NaturalLog.Update()
end

function SetupPanel()
	window.HistoryBox:SetText( towstring( NaturalLog.BasicVars.LogLimit ) )
	window.EditNumLines:SetText( towstring( NaturalLog.BasicVars.LineCount ) )
	window.EditAlpha:SetText( towstring( NaturalLog.BasicVars.Alpha * 100 ) )
	window.SliderAlpha:SetValue( towstring( NaturalLog.BasicVars.Alpha * 100 ) )
	window.SliderWidth:SetValue( NaturalLog.BasicVars.BarWidth )
	window.SliderHeight:SetValue(  NaturalLog.BasicVars.BarHeight )
	
	if NaturalLog.BasicVars.ArraySideways then
		window.OrientationBox:SelectIndex(2)
	else
		window.OrientationBox:SelectIndex(1)
	end
	if NaturalLog.BasicVars.FlowUp then
		window.DirectionBox:SelectIndex(1)
	else
		window.DirectionBox:SelectIndex(2)
	end
	
	window.IconAlignBox:SelectIndex(NaturalLog.BasicVars.IconSpacing)
	
	lnConfig.GeneralUpdate()
end


function lnConfig.GeneralUpdate()
    if lnConfig.ActivePanel ~= panel.id then return end
	
    local sAlpha, eAlpha, sWidth, eWidth, sHeight, eHeight
    local updated = false
    local eupdated = false
	
	local w,h = GetScreenResolution()
	
    -- Check to see if selectors have been updated
    sAlpha = math.floor(window.SliderAlpha:GetValue())
    eAlpha = tonumber(window.EditAlpha:GetText()) or sAlpha
	
    sWidth = math.floor(window.SliderWidth:GetValue() * w)
    eWidth = tonumber(window.EditWidth:GetText()) or sWidth
	
    sHeight = math.floor(window.SliderHeight:GetValue() * h)
    eHeight = tonumber(window.EditHeight:GetText()) or sHeight
    
	if window.OrientationBox:SelectedIndex() ~= gOrient then
		gOrient = window.OrientationBox:SelectedIndex()
		if gOrient == 1 then
			local prev = window.DirectionBox:SelectedIndex()
			window.DirectionBox:Clear()
			window.DirectionBox:Add(L"Up")
			window.DirectionBox:Add(L"Down")
			window.DirectionBox:SelectIndex(prev)
		else
			local prev = window.DirectionBox:SelectedIndex()
			window.DirectionBox:Clear()
			window.DirectionBox:Add(L"Left")
			window.DirectionBox:Add(L"Right")
			window.DirectionBox:SelectIndex(prev)
		end
	end
	
    if sAlpha ~= gAlpha then
        gAlpha = sAlpha
        updated = true
    elseif eAlpha ~= gAlpha then
        gAlpha = eAlpha
        eupdated = true
    end
	
    if sWidth ~= gWidth then
        gWidth = sWidth
        updated = true
    elseif eWidth ~= gWidth then
        gWidth = eWidth
        eupdated = true
    end
	
    if sHeight ~= gHeight then
        gHeight = sHeight
        updated = true
    elseif eHeight ~= gHeight then
        gHeight = eHeight
        eupdated = true
    end
	
    if updated or eupdated then
	    NaturalLog.Set("alpha", gAlpha / 100)
		
	    window.SliderAlpha:SetValue(gAlpha)
	    window.SliderHeight:SetValue(gHeight / h)
	    window.SliderWidth:SetValue(gWidth / w)
		
	    if not eupdated then
	        window.EditAlpha:SetText(towstring(gAlpha))
	        window.EditHeight:SetText(towstring(gHeight))
	        window.EditWidth:SetText(towstring(gWidth))
	    end
	end
end

function RevertPanel()
	SetupPanel()
	ApplyPanel()
end

-- Actually add the panel
panel.create = CreatePanel
panel.revert = RevertPanel
panel.id = lnConfig.AddPanel(panel)
if not lnBar then lnBar = {} end

lnBar = Frame:Subclass("lnBar")

local bCount = 1

function lnBar:New(logger, windowname, entries)
	local b = setmetatable({}, self)
	b.window = windowname or "LNBAR"..bCount
	CreateWindowFromTemplate( b.window, "LN_BAR", "Root" )
	b.logger = logger
	b.entrycount = entries
	b.offset = 0 
	b.scroll = 0 
	b.spacing = 3
	b.scrollsize = 15
	b.topstate = 1
	b.bottomstate = 1
	
	b:SetSize()
	b:SetAlpha(.75)
	b:SetEntries(entries)
	
	bCount = bCount + 1
	return( b )
end


function lnBar:SetSize(w,h,redraw)
	if not self.window then return end
	
	self.width = w or .8
	self.height = h or .025
	
	local w,h = GetScreenResolution()
	self.width = self.width * w / (h*SystemData.Settings.Interface.globalUiScale*.000833333)
	self.height = self.height / (SystemData.Settings.Interface.globalUiScale*.000833333)
	
	WindowSetDimensions(self.window, self.width, self.height)
	if redraw == nil or redraw then self:SetEntries() end
end

function lnBar:AddEntry(entry)
	if not self.window then return end
	if self.logger:AddEntry(entry, self.scroll) then 
		self:ShiftDown()
		if self.topstate == 1 and self.logger.activecount > self.entrycount then
			self:SetScroll( 1,2 )
		end
	else self.scroll = self.scroll + 1 end
end

function lnBar:SetScroll( loc, state )
	
	local lead
	local colors = {}
	if state == 1 then colors = NaturalLog.BasicVars.ScrollDisabled else colors = NaturalLog.BasicVars.ScrollEnabled end
	if ( loc == 1 ) == NaturalLog.BasicVars.FlowUp or ( loc == 2 ) ~= NaturalLog.BasicVars.FlowUp then lead = "Top" self.topstate = state else lead ="Bottom" self.bottomstate = state end
	
	WindowSetTintColor( self.window..lead.."Left", colors[1], colors[2], colors[3] )
	WindowSetTintColor( self.window..lead.."Center", colors[1], colors[2], colors[3] )
	WindowSetTintColor( self.window..lead.."Right", colors[1], colors[2], colors[3] )
	
	WindowSetAlpha( self.window..lead.."Left", colors[4] )
	WindowSetAlpha( self.window..lead.."Center", colors[4] )
	WindowSetAlpha( self.window..lead.."Right", colors[4] )
end

function lnBar:Trim()
	if not self.window then return end
	self.logger:Trim( self.scroll )
end

function lnBar:Scroll(delta)
	if not self.window then return end
	if delta > 0 then
		self:SetScroll(1,2)
		if self.scroll == 1 then self:SetScroll(2,1) end
		if self.scroll > 0 then
			self.scroll = self.scroll - 1
			self:ShiftDown()
		end
	elseif self.scroll < self.logger.activecount - self.entrycount then
		self:SetScroll(2,2)
		if self.scroll == self.logger.activecount - self.entrycount - 1 then self:SetScroll(1,1) end
		self.scroll = self.scroll + 1
		self:ShiftUp()
	end
end

function lnBar:ScrollTo(delta)
	if not self.window then return end
	if not NaturalLog.BasicVars.FlowUp then delta = 0 - delta end
	if delta > 0 then
		if self.scroll > 0 then
			self.scroll = 0
			self:SetEntries()
			self:SetScroll(2,1)
			self:SetScroll(1,2)
		end
	elseif self.scroll < self.logger.activecount - self.entrycount then
		self.scroll = self.logger.activecount - self.entrycount
		self:SetEntries()
		self:SetScroll(1,1)
		self:SetScroll(2,2)
	end
end

-- Bar shift up
function lnBar:ShiftDown()
	if not self.window then return end
	self.offset = self.offset + 1
	if self.offset >= self.entrycount then self.offset = 0 end
	for i = 1, self.entrycount do
		self.entries[i]:SetLoc(self:GetLoc(i + self.offset))
	end
	self.entries[1 - self.offset]:SetupEntry(self.logger:GetEntry(self.scroll))
end

function lnBar:ShiftUp()
	if not self.window then return end
	self.offset = self.offset - 1
	if self.offset < 0 then self.offset = self.entrycount - 1 end
	for i = 1, self.entrycount do
		self.entries[i]:SetLoc(self:GetLoc(i + self.offset))
	end
	self.entries[self.entrycount - self.offset]:SetupEntry(self.logger:GetEntry(self.entrycount + self.scroll - 1))
	
end

function lnBar:GetLoc(line)
	return self.locs[line].x, self.locs[line].y
end

function lnBar:SetEntries(entries)
	if not self.window then return end
	if not entries then entries = self.entrycount end
	if not self.entries then self.entries = {} end
	if not self.locs then self.locs = {} end
	self.offset = 0
	while self.entrycount > entries do
		self.entries[self.entrycount]:Delete()
		self.entries[self.entrycount] = nil
		self.entrycount = self.entrycount - 1
	end
	
	self.entrycount = entries
	if NaturalLog.BasicVars.ArraySideways then
		self.eHeight = self.height
		self.eWidth = self.width / self.entrycount
	else
		self.eHeight = self.height / self.entrycount
		self.eWidth = self.width
	end
	
	for i = 1, entries do
		if not self.entries[i] then self.entries[i] = lnEntry:New(self.window, self.window.."Entry") end
		local loc
		if NaturalLog.BasicVars.FlowUp then loc = entries - i else loc = i - 1 end
		if NaturalLog.BasicVars.ArraySideways then
			self.locs[i] = { ["x"] = loc * self.eWidth, ["y"] = 0 }
		else
			self.locs[i] = { ["x"] = 0, ["y"] = loc * self.eHeight }
		end
		
		self.entries[i]:NilOut()
		self.entries[i].spacing = self.spacing
		self.entries[i]:SetLoc(self:GetLoc(i))
		self.entries[i]:SetSize( self.eWidth, self.eHeight )
		self.entries[i]:SetupEntry( self.logger:GetEntry(i + self.scroll - 1) )
		 -- help with the barrel shifter (less computation of offsets)
		self.locs[i+self.entrycount] = self.locs[i]
		self.entries[i-self.entrycount] = self.entries[i]
	end
end

function lnBar:GetLog(wnd)
	for i = 1, self.entrycount do
		if self.entries[i].window == wnd then
			local off = i + self.offset
			if off > self.entrycount then off = off - self.entrycount end 
			local entry = self.logger:GetEntry(off + self.scroll - 1)
			if entry then return entry.logtext end
		end
	end
end 

function lnBar:SetAlpha(alpha)
	if not self.window then return end
	WindowSetAlpha( self.window.."Background", alpha )
end 

function lnBar:SetScrollType(vertical)
	if vertical then
		WindowClearAnchors( self.window.."TopLeft" )
		WindowClearAnchors( self.window.."TopCenter" )
		WindowClearAnchors( self.window.."TopRight" )
		WindowClearAnchors( self.window.."BottomLeft" )
		WindowClearAnchors( self.window.."BottomCenter" )
		WindowClearAnchors( self.window.."BottomRight" )
		WindowAddAnchor( self.window.."TopLeft", "topleft", self.window, "topleft", 0, 0)
		WindowAddAnchor( self.window.."TopCenter", "top", self.window, "top", 0, 0)
		WindowAddAnchor( self.window.."TopRight", "topright", self.window, "topright", 0, 0)
		WindowAddAnchor( self.window.."BottomLeft", "bottomleft", self.window, "bottomleft", 0, 0)
		WindowAddAnchor( self.window.."BottomCenter", "bottom", self.window, "bottom", 0, 0)
		WindowAddAnchor( self.window.."BottomRight", "bottomright", self.window, "bottomright", 0, 0)
		DynamicImageSetTexture( self.window.."TopLeft", "lnscroll", 0, 31 )
		DynamicImageSetTexture( self.window.."TopCenter", "lnscroll", 0, 31 )
		DynamicImageSetTexture( self.window.."TopRight", "lnscroll", 0, 31 )
		DynamicImageSetTexture( self.window.."BottomLeft", "lnscroll", 0, 0 )
		DynamicImageSetTexture( self.window.."BottomCenter", "lnscroll", 0, 0 )
		DynamicImageSetTexture( self.window.."BottomRight", "lnscroll", 0, 0 )
		DynamicImageSetTextureDimensions( self.window.."TopLeft", 32, -32 )
		DynamicImageSetTextureDimensions( self.window.."TopCenter", 32, -32 )
		DynamicImageSetTextureDimensions( self.window.."TopRight", 32, -32 )
		DynamicImageSetTextureDimensions( self.window.."BottomLeft", 32, 32 )
		DynamicImageSetTextureDimensions( self.window.."BottomCenter", 32, 32 )
		DynamicImageSetTextureDimensions( self.window.."BottomRight", 32, 32 )
	else
		WindowClearAnchors( self.window.."TopLeft" )
		WindowClearAnchors( self.window.."TopCenter" )
		WindowClearAnchors( self.window.."TopRight" )
		WindowClearAnchors( self.window.."BottomLeft" )
		WindowClearAnchors( self.window.."BottomCenter" )
		WindowClearAnchors( self.window.."BottomRight" )
		WindowAddAnchor( self.window.."TopLeft", "topleft", self.window, "topleft", 0, 0)
		WindowAddAnchor( self.window.."TopCenter", "left", self.window, "left", 0, 0)
		WindowAddAnchor( self.window.."TopRight", "bottomleft", self.window, "bottomleft", 0, 0)
		WindowAddAnchor( self.window.."BottomLeft", "topright", self.window, "topright", 0, 0)
		WindowAddAnchor( self.window.."BottomCenter", "right", self.window, "right", 0, 0)
		WindowAddAnchor( self.window.."BottomRight", "bottomright", self.window, "bottomright", 0, 0)
		DynamicImageSetTexture( self.window.."TopLeft", "lnscroll", 32, 0 )
		DynamicImageSetTexture( self.window.."TopCenter", "lnscroll", 32, 0 )
		DynamicImageSetTexture( self.window.."TopRight", "lnscroll", 32, 0 )
		DynamicImageSetTexture( self.window.."BottomLeft", "lnscroll", 63, 0 )
		DynamicImageSetTexture( self.window.."BottomCenter", "lnscroll", 63, 0 )
		DynamicImageSetTexture( self.window.."BottomRight", "lnscroll", 63, 0 )
		DynamicImageSetTextureDimensions( self.window.."TopLeft", 32, 32 )
		DynamicImageSetTextureDimensions( self.window.."TopCenter", 32, 32 )
		DynamicImageSetTextureDimensions( self.window.."TopRight", 32, 32 )
		DynamicImageSetTextureDimensions( self.window.."BottomLeft", -32, 32 )
		DynamicImageSetTextureDimensions( self.window.."BottomCenter", -32, 32 )
		DynamicImageSetTextureDimensions( self.window.."BottomRight", -32, 32 )
	end
end

function lnBar:SetScrolls(states)
	if not self.window then return end
	WindowSetShowing( self.window.."TopLeft", states[1] )
	WindowSetShowing( self.window.."TopCenter", states[2] )
	WindowSetShowing( self.window.."TopRight", states[3] )
	WindowSetShowing( self.window.."BottomLeft", states[4] )
	WindowSetShowing( self.window.."BottomCenter", states[5] )
	WindowSetShowing( self.window.."BottomRight", states[6] )
end

function lnBar:SetScrollSize(size)
	if not self.window then return end
	WindowSetDimensions( self.window.."TopLeft", size, size )
	WindowSetDimensions( self.window.."TopCenter", size, size )
	WindowSetDimensions( self.window.."TopRight", size, size )
	WindowSetDimensions( self.window.."BottomLeft", size, size )
	WindowSetDimensions( self.window.."BottomCenter", size, size )
	WindowSetDimensions( self.window.."BottomRight", size, size )
end

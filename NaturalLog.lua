NaturalLog = {}
NaturalLog.BasicVars = {}

event = { ["iconNum"]=4503, ["text"]=L"Absorb" }

local mainLog, mainBar

function NaturalLog.OnLoad()
	mainLog = lnLogger:New()
	mainBar = lnBar:New(mainLog, "lnBar", 40)
	mainTooltip = lnTooltip:New("Tooltippa")
	lnHandler.Init(mainBar,mainTooltip)
	lnConfig.Init()
	
	mainBar:SetScroll(1,1)
	mainBar:SetScroll(2,1)
end 

function NaturalLog.LoadSettings()
	mainBar.spacing = NaturalLog.BasicVars.IconSpacing
	mainLog.limit = NaturalLog.BasicVars.LogLimit
	mainBar:SetAlpha( NaturalLog.BasicVars.Alpha )
	mainBar:SetSize( NaturalLog.BasicVars.BarWidth, NaturalLog.BasicVars.BarHeight, false )
	mainBar:SetScrolls( NaturalLog.BasicVars.ScrollShow )
	mainBar:SetScrollType(not NaturalLog.BasicVars.ArraySideways)
	mainBar:SetEntries( NaturalLog.BasicVars.LineCount )
	mainBar:Trim()
end

function NaturalLog.Update()
	NaturalLog.Set("")
end

function NaturalLog.ScrollToTop()
	mainBar:ScrollTo(-1)
end
function NaturalLog.ScrollToBottom()
	mainBar:ScrollTo(1)
end

function NaturalLog.Assign(item, ...)
	item = item:lower()
	if item == "size" then
		w,h = ...
		if tonumber(w) <= 1 and tonumber(h) <= 1 then
			NaturalLog.BasicVars.BarWidth, NaturalLog.BasicVars.BarHeight = w,h
			mainBar:SetSize(w,h,false)
		end
	elseif item == "lines" then
		if tonumber((...)) > 0 then
			NaturalLog.BasicVars.LineCount = (...)
		end
	elseif item == "alpha" then
		if tonumber((...)) >= 0 then
			NaturalLog.BasicVars.Alpha = (...)
			mainBar:SetAlpha(...)
		end
	elseif item == "loglimit" then
		if tonumber((...)) >= 0 then
			NaturalLog.BasicVars.LogLimit = (...)
			mainLog.limit = (...)
			mainBar:Trim()
		end
	elseif item == "orient" then
		NaturalLog.BasicVars.ArraySideways = (...)
		mainBar:SetScrollType(not (...))
	elseif item == "direction" then
		if NaturalLog.BasicVars.ScrollEnabled ~= nil then
			if NaturalLog.BasicVars.FlowUp == true and (...) == false then
				local temptop = mainBar.topstate
				local tempbottom = mainBar.bottomstate
				mainBar:SetScroll(1,tempbottom)
				mainBar:SetScroll(2,temptop)
			elseif NaturalLog.BasicVars.FlowUp == false and (...) == true then
				local temptop = mainBar.topstate
				local tempbottom = mainBar.bottomstate
				mainBar:SetScroll(2,tempbottom)
				mainBar:SetScroll(1,temptop)
			end
		end
		NaturalLog.BasicVars.FlowUp = (...)
	elseif item == "spacing" then
		if tonumber((...)) >= 1 and tonumber((...)) <= 3 then
			NaturalLog.BasicVars.IconSpacing = (...)
			mainBar.spacing = NaturalLog.BasicVars.IconSpacing
		end
	elseif item == "movable" then
		WindowSetMovable(mainBar.window, (...))
	elseif item == "activecolor" then
		local r,g,b,a = ...
		NaturalLog.BasicVars.ScrollEnabled = {r,g,b,a}
		if mainBar.topstate == 2 and NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(1,2) end
		if mainBar.topstate == 2 and not NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(2,2) end
		if mainBar.bottomstate == 2 and NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(2,2) end
		if mainBar.bottomstate == 2 and not NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(1,2) end
	elseif item == "inactivecolor" then
		local r,g,b,a = ...
		NaturalLog.BasicVars.ScrollDisabled = {r,g,b,a}
		if mainBar.topstate == 1 and NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(1,1) end
		if mainBar.topstate == 1 and not NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(2,1) end
		if mainBar.bottomstate == 1 and NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(2,1) end
		if mainBar.bottomstate == 1 and not NaturalLog.BasicVars.FlowUp then mainBar:SetScroll(1,1) end
	elseif item == "scroll" then
		NaturalLog.BasicVars.ScrollShow = (...)
		mainBar:SetScrolls( NaturalLog.BasicVars.ScrollShow )
	elseif item == "scrollsize" then
		if tonumber((...)) >= 0 then
			NaturalLog.BasicVars.ScrollSize = (...)
			mainBar:SetScrollSize( NaturalLog.BasicVars.ScrollSize )
		end
	else
		d(...)
	end
end

function NaturalLog.Set(item, ...)
	NaturalLog.Assign(item, ...)
	mainBar:SetEntries(NaturalLog.BasicVars.LineCount)
end 
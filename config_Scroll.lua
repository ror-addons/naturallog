-------------------------------------
--  Natural Log Config  --  version: 1.0.1
--  
--  Config panel for scroll buttons
--
--     Written By NigelTufnel ( Adam.Dew@gmail.com )
-------------------------------------

if not lnConfig then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel, RevertPanel, ScrollUpdate
local panel = {}
local active = {["select"] = 0, ["color"] = {255,255,255,100},}
-- Our panel window
local window
local settingfont = "font_clear_small_bold"

panel.title = L"Scroll Buttons"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if window and window.name then return window end
    
    window = LibGUI("Blackframe")
    window:Resize(420, 390)
    
    local e
    
    -- Title label
    e = window("Label")
    e:Resize(480)
    e:AnchorTo(window, "top", "top", 0, 8)
    e:Font("font_clear_large_bold")
    e:SetText(L"Scroll Button Settings")
    window.Title = e
	
	-- scrollbar example image
	e = window("Image")
	e:Resize(32,32)
	e:AnchorTo(window, "topleft", "topleft", 32, 113)
	e:Texture("lnscroll", 0, 0)
	e:TexDims(32, 32)
	WindowSetTintColor(e.name, 0, 255, 255)
	window.ScrollPreview = e
	
	
	
	
    -- Color label
    e = window("Label")
    e:Resize(90)
    e:AnchorTo(window, "topleft", "topleft", 25, 43)
    e:Font("font_clear_large_bold")
	e:Color(255,255,255)
    e:SetText(L"Color")
    e:Align("bottomleft")
    window.ColorLabel = e
	
	-- top state
    e = window("combobox")
    e:AnchorTo(window, "topleft", "topleft", 152, 45)
	e:Add(L"Active Colors")
	e:Add(L"Inactive Colors")
	e:SelectIndex(1)
    window.ScrollPick = e
	
	
	-- RED
    e = window("Slider")
    e:AnchorTo(window.ColorLabel, "bottomleft", "topleft", 122, 7)
    e:SetRange(0,255)
    window.SliderRed = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderRed, "left", "right", -7, 0)
    e:SetText("Red")
	e:Font(settingfont)
    window.LabelRed = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderRed, "right", "left", 5, 0)
    e:SetText(255)
    window.EditRed = e
	
	-- GREEN
    e = window("Slider")
    e:AnchorTo(window.SliderRed, "bottom", "top", 0, 0)
    e:SetRange(0,255)
    window.SliderGreen = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderGreen, "left", "right", -1, 0)
    e:SetText("Green")
	e:Font(settingfont)
    window.LabelGreen = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderGreen, "right", "left", 5, 0)
    e:SetText(255)
    window.EditGreen = e
	
	-- BLUE
    e = window("Slider")
    e:AnchorTo(window.SliderGreen, "bottom", "top", 0, 0)
    e:SetRange(0,255)
    window.SliderBlue = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderBlue, "left", "right", -4, 0)
    e:SetText("Blue")
	e:Font(settingfont)
    window.LabelBlue = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderBlue, "right", "left", 5, 0)
    e:SetText(255)
    window.EditBlue = e
	
	-- ALHPA
    e = window("Slider")
    e:AnchorTo(window.SliderBlue, "bottom", "top", 0, 0)
    e:SetRange(0,100)
    window.SliderAlpha = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderAlpha, "left", "right", -4, 0)
    e:SetText("Opacity")
	e:Font(settingfont)
    window.LabelAlpha = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderAlpha, "right", "left", 5, 0)
    e:SetText(100)
    window.EditAlpha = e
	
	
		
	
	
	
	
	
	
    -- display label
    e = window("Label")
    e:Resize(90)
    e:AnchorTo(window, "topleft", "topleft", 25, 215)
    e:Font("font_clear_large_bold")
	e:Color(255,255,255)
    e:SetText(L"Display")
    e:Align("bottomleft")
    window.ScrollState = e
	
	
	-- top
    e = window("Label")
    e:Resize(80)
    e:Align("rightcenter")
    e:AnchorTo(window.ScrollState, "bottomleft", "topleft",5, 4)
    e:SetText(L"Top")
	e:Font(settingfont)
    window.TopState = e
	-- bottom
    e = window("Label")
    e:Resize(80)
    e:Align("rightcenter")
    e:AnchorTo(window.TopState, "bottomleft", "topleft",0, 0)
    e:SetText(L"Bottom")
	e:Font(settingfont)
    window.BottomState = e
	
	
	
	-- left
    e = window("Label")
    e:Resize(65)
    e:Align("center")
    e:AnchorTo(window.ScrollState, "right", "left",15, 5)
    e:SetText(L"Left")
	e:Font(settingfont)
    window.LeftLabel = e
	-- center
    e = window("Label")
    e:Resize(65)
    e:Align("center")
    e:AnchorTo(window.LeftLabel, "right", "left",0, 0)
    e:SetText(L"Center")
	e:Font(settingfont)
    window.CenterLabel = e
	-- right
    e = window("Label")
    e:Resize(65)
    e:Align("center")
    e:AnchorTo(window.CenterLabel, "right", "left",0, 0)
    e:SetText(L"Right")
	e:Font(settingfont)
    window.RightLabel = e
	
	-- top boxes
    e = window("CheckBox")
    e:AnchorTo(window.LeftLabel, "bottom", "top",0, 5)
    window.TopLeftBox = e
    e = window("CheckBox")
    e:AnchorTo(window.CenterLabel, "bottom", "top",0, 5)
    window.TopCenterBox = e
    e = window("CheckBox")
    e:AnchorTo(window.RightLabel, "bottom", "top",0, 5)
    window.TopRightBox = e
	
	-- bottom boxes
    e = window("CheckBox")
    e:AnchorTo(window.TopLeftBox, "bottom", "top",0, 5)
    window.BottomLeftBox = e
    e = window("CheckBox")
    e:AnchorTo(window.TopCenterBox, "bottom", "top",0, 5)
    window.BottomCenterBox = e
    e = window("CheckBox")
    e:AnchorTo(window.TopRightBox, "bottom", "top",0, 5)
    window.BottomRightBox = e
	
	
	-- size
    e = window("Label")
    e:Resize(105)
    e:Align("center")
    e:AnchorTo(window.TopRightBox, "right", "left",5, -20)
    e:SetText(L"Size")
	e:Font(settingfont)
    window.SizeLabel = e
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SizeLabel, "bottom", "top", 2, 0)
    e:SetText(16)
    window.EditSize = e
	
	
	
	
	
	
    -- Apply button
    e = window("Button")
    e:Resize(175)
    e:SetText(L"Apply")
    e:AnchorTo(window, "bottomleft", "bottomleft", 25, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    window.ButtonApply = e
    
    -- Revert button
    e = window("Button")
    e:Resize(175)
    e:SetText(L"Undo")
    e:AnchorTo(window, "bottomright", "bottomright", -25, -20)
    e.OnLButtonUp = function() RevertPanel() end
    window.ButtonRevert = e
    
    return window
end

function ApplyPanel()	
	
	if window.ScrollPick:SelectedIndex() == 1 then 
		NaturalLog.Assign("activecolor", active.color[1], active.color[2], active.color[3], active.color[4] / 100)
	else 
		NaturalLog.Assign("inactivecolor", active.color[1], active.color[2], active.color[3], active.color[4] / 100)
	end
	
	NaturalLog.Assign( "scroll", {window.TopLeftBox:GetValue(), window.TopCenterBox:GetValue(), window.TopRightBox:GetValue(), 
							  	  window.BottomLeftBox:GetValue(), window.BottomCenterBox:GetValue(), window.BottomRightBox:GetValue()} )
	NaturalLog.Assign( "scrollsize", tonumber(window.EditSize:GetText()) )
	
	NaturalLog.Update()
end

function UpdatePanel(self, key, value)
end

function RevertPanel()
	active.select = 0
	
	window.EditSize:SetText( towstring(NaturalLog.BasicVars.ScrollSize) )
	
	window.TopLeftBox:SetValue( NaturalLog.BasicVars.ScrollShow[1] )
	window.TopCenterBox:SetValue( NaturalLog.BasicVars.ScrollShow[2] )
	window.TopRightBox:SetValue( NaturalLog.BasicVars.ScrollShow[3] )
	window.BottomLeftBox:SetValue( NaturalLog.BasicVars.ScrollShow[4] )
	window.BottomCenterBox:SetValue( NaturalLog.BasicVars.ScrollShow[5] )
	window.BottomRightBox:SetValue( NaturalLog.BasicVars.ScrollShow[6] )
	
	ScrollUpdate()
end
function lnConfig.ScrollUpdate()
	if lnConfig.ActivePanel ~= panel.id then return end
	ScrollUpdate()
end
function ScrollUpdate()

    local sRed, sGreen, sBlue, sAlpha
    local eRed, eGreen, eBlue, eAlpha
    local updated = false
    local eupdated = false
	
	
	if active.select ~= window.ScrollPick:SelectedIndex() then
		active.select = window.ScrollPick:SelectedIndex()
		
		local nRed, nGreen, nBlue, nAlpha
		if window.ScrollPick:SelectedIndex() == 1 then
			nRed, nGreen, nBlue, nAlpha = unpack(NaturalLog.BasicVars.ScrollEnabled)
		else
			nRed, nGreen, nBlue, nAlpha = unpack(NaturalLog.BasicVars.ScrollDisabled)
		end
		
		nAlpha = math.floor(nAlpha*100)
		
        window.SliderRed:SetValue(nRed)
        window.SliderGreen:SetValue(nGreen)
        window.SliderBlue:SetValue(nBlue)
        window.SliderAlpha:SetValue(nAlpha)
    
        window.EditRed:SetText(towstring(nRed))
        window.EditGreen:SetText(towstring(nGreen))
        window.EditBlue:SetText(towstring(nBlue))
        window.EditAlpha:SetText(towstring(nAlpha))
		
		updated = true
	end
	
	
    -- Check to see if color selectors have been updated
    sRed = math.floor(window.SliderRed:GetValue())
    sGreen = math.floor(window.SliderGreen:GetValue())
    sBlue = math.floor(window.SliderBlue:GetValue())
    sAlpha = math.floor(window.SliderAlpha:GetValue())
        
    eRed = tonumber(window.EditRed:GetText()) or sRed
    eGreen = tonumber(window.EditGreen:GetText()) or sGreen
    eBlue = tonumber(window.EditBlue:GetText()) or sBlue
    eAlpha = tonumber(window.EditAlpha:GetText()) or sAlpha
    
    if sRed ~= active.color[1] then
        active.color[1] = sRed
        updated = true
    elseif eRed ~= active.color[1] then
        active.color[1] = eRed
        eupdated = true
    end
    
    if sGreen ~= active.color[2] then
        active.color[2] = sGreen
        updated = true
    elseif eGreen ~= active.color[2] then
        active.color[2] = eGreen
        eupdated = true
    end
    
    if sBlue ~= active.color[3] then
        active.color[3] = sBlue
        updated = true
    elseif eBlue ~= active.color[3] then
        active.color[3] = eBlue
        eupdated = true
    end
	
    if sAlpha ~= active.color[4] then
        active.color[4] = sAlpha
        updated = true
    elseif eAlpha ~= active.color[4] then
        active.color[4] = eAlpha
        eupdated = true
    end
	
    if updated or eupdated then
	    window.SliderRed:SetValue(active.color[1])
	    window.SliderGreen:SetValue(active.color[2])
	    window.SliderBlue:SetValue(active.color[3])
	    window.SliderAlpha:SetValue(active.color[4])
	    
	    if not eupdated then
	        window.EditRed:SetText(towstring(active.color[1]))
	        window.EditGreen:SetText(towstring(active.color[2]))
	        window.EditBlue:SetText(towstring(active.color[3]))
	        window.EditAlpha:SetText(towstring(active.color[4]))
	    end
		WindowSetTintColor(window.ScrollPreview.name, active.color[1], active.color[2], active.color[3])
		WindowSetAlpha(window.ScrollPreview.name, active.color[4]/100)
	end
end

-- Actually add the panel
panel.create = CreatePanel
panel.revert = RevertPanel
panel.id = lnConfig.AddPanel(panel)
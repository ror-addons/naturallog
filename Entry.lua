if not lnEntry then lnEntry = {} end

lnEntry = Frame:Subclass("lnEntry")

local eCount = 1

local LabelSetText = LabelSetText
local WindowAddAnchor = WindowAddAnchor
local WindowClearAnchors = WindowClearAnchors
local WindowSetDimensions = WindowSetDimensions
local DynamicImageSetTexture = DynamicImageSetTexture

function lnEntry:New(parent, windowname)
	local e = setmetatable({}, self)
	
	e.parent = parent
	e.window = windowname..eCount or "LNEVENT"..eCount
	
	CreateWindowFromTemplate( e.window, "LN_ENTRY", parent )
	
	eCount = eCount + 1
	return e
end

function lnEntry:SetupEntry(event)
	if not self.window  or not event then return end
	if self.event == nil then self.event = {} end
	
	self.tx = event.tx or 0
	self.ty = event.ty or 0
	if self.tw ~= event.tw or self.th ~= event.th then self:SetTexDim(event.tw,event.th) end
	if self.sw ~= event.sw or self.sh ~= event.sh then self:SetTexScl(event.sw,event.sh) end

	if self.etype ~= event.etype then self:SetTextType(event.etype) end
	if self.spacing == 3 and self.iAlign ~= NaturalLog.LogOptions[event.etype][8] then self:SetIconAlign(NaturalLog.LogOptions[event.etype][8]) end
	if self.tAlign ~= NaturalLog.LogOptions[event.etype][9] then self:SetTextAlign(NaturalLog.LogOptions[event.etype][9]) end
	self:SetIcon(event.iconNum)
	self:SetText(event.text)
end

function lnEntry:SetText(text)
	if not self.window then return end
	self.text = text or towstring(NaturalLog.LogOptions[self.etype][7])
	LabelSetText(self.window.."Value", NaturalLog.LogOptions[self.etype][4]..self.text..NaturalLog.LogOptions[self.etype][5])
end

function lnEntry:SetIcon(iconNum)
	if not self.window or not iconNum then return end
	local iconTex = ""
	if iconNum ~= 0 then iconTex = GetIconData( iconNum ) end
	DynamicImageSetTexture( self.window.."Icon", iconTex, self.tx, self.ty )
end

function lnEntry:SetTextType(etype)
	if not self.window then return end
	self.etype = etype
	if etype then
		LabelSetTextColor( self.window.."Value", NaturalLog.LogOptions[etype][1], NaturalLog.LogOptions[etype][2], NaturalLog.LogOptions[etype][3] )
		LabelSetFont( self.window.."Value", lnHandler.FontTypes[NaturalLog.LogOptions[etype][6]][1], 20)
	else
		LabelSetTextColor( self.window.."Value", 255,255,255 )
		LabelSetFont( self.window.."Value", lnHandler.FontTypes[3][1], 20)
	end
	-- Set colors/font types
end

function lnEntry:SetIconAlign(align)
	if not self.window then return end
	self.iAlign = align or "right"
	if self.iAlign == "left" then
		WindowClearAnchors( self.window.."Icon" )
		WindowAddAnchor( self.window.."Icon", "left", self.window, "center", self.height / 2, 0 )
	else
		WindowClearAnchors( self.window.."Icon" )
		WindowAddAnchor( self.window.."Icon", "right", self.window, "center", -self.height / 2, 0 )
	end
end

function lnEntry:SetTextAlign(align)
	if not self.window then return end
	self.tAlign = align or "rightcenter"
	LabelSetTextAlign(self.window.."Value", self.tAlign)
end

function lnEntry:SetSize(width, height)
	if not self.window then return end
	self.width = width or 120
	self.height = height or 30
	
	if self.spacing == 1 then
		WindowClearAnchors( self.window.."Icon" )
		WindowClearAnchors( self.window.."Value" )
		WindowAddAnchor( self.window.."Icon", "left", self.window, "center", self.height / 2, 0 )
		WindowAddAnchor( self.window.."Value", "topleft", self.window, "topleft", self.height + 3, 0 )
		WindowAddAnchor( self.window.."Value", "bottomright", self.window, "bottomright", -3, 0 )
	elseif self.spacing == 2 then 
		WindowClearAnchors( self.window.."Icon" )
		WindowClearAnchors( self.window.."Value" )
		WindowAddAnchor( self.window.."Icon", "right", self.window, "center", -self.height / 2, 0 )
		WindowAddAnchor( self.window.."Value", "topleft", self.window, "topleft", 3, 0 )
		WindowAddAnchor( self.window.."Value", "bottomright", self.window, "bottomright", -self.height - 3, 0 )
	else
		WindowClearAnchors( self.window.."Value" )
		WindowAddAnchor( self.window.."Value", "topleft", self.window, "topleft", self.height + 3, 0 )
		WindowAddAnchor( self.window.."Value", "bottomright", self.window, "bottomright", -self.height - 3, 0 )
	end
	
	WindowSetDimensions( self.window, self.width, self.height )
	WindowSetDimensions( self.window.."Icon", self.height, self.height )
end 

function lnEntry:SetLoc(x, y)
	if not self.window then return end
	WindowClearAnchors(self.window)
	WindowAddAnchor(self.window, "topleft", self.parent, "topleft", x, y)
end 

function lnEntry:SetTexDim(tw,th)
	if not self.window then return end
	DynamicImageSetTextureDimensions( self.window.."Icon", (tw or 64), (th or 64))
	self.tw = tw
	self.th = th
end

function lnEntry:SetTexScl(sw,sh)
	if not self.window then return end
	WindowSetDimensions( self.window.."Icon", (sw or 1) * self.height, (sh or 1) * self.height )
	self.sw = sw
	self.sh = sh
end

function lnEntry:Delete()
	DestroyWindow(self.window)
end

function lnEntry:NilOut()
	self.tAlign = nil self.iAlign = nil self.etype = nil
	if self.width then self:SetTexDim() self:SetTexScl() end
	LabelSetText(self.window.."Value", L"")
	DynamicImageSetTexture( self.window.."Icon", "", 0,0 )
end 
-------------------------------------
--  Natural Log Config  --  version: 1.0.1
--  
--  Config panel for default settings
--
--     Written By NigelTufnel ( Adam.Dew@gmail.com )
-------------------------------------

if not lnConfig then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

-- Our panel window
local window
local wx = 312
local wy = 380

panel.title = L"Defaults"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if window and window.name then return window end
    
    window = LibGUI("Blackframe")
    window:Resize(wx, wy + 45)
    
    local e
    
    -- Title label
    e = window("Label")
    e:Resize(480)
    e:AnchorTo(window, "top", "top", 0, 8)
    e:Font("font_clear_large_bold")
    e:SetText(L"Apply Default Layouts")
    window.Title = e
	
    e = window("Button")
    e:Resize(275)
    e:SetText(L"Vertical Default")
    e:AnchorTo(window, "top", "top", 0, 65)
    e.OnLButtonUp = function() 
		window.ApplyLabel:Show() window.Yes:Show() window.No:Show() 
		window.Yes.OnLButtonUp = function() 
			ApplyPanel() lnConfig.SetVertical()
		end
		window:Resize(wx, wy + 45)
	end
    window.SetVertical = e
    
    e = window("Button")
    e:Resize(275)
    e:SetText(L"Horizontal Default")
    e:AnchorTo(window.SetVertical, "bottom", "top", 0, 10)
    e.OnLButtonUp = function() 
		window.ApplyLabel:Show() window.Yes:Show() window.No:Show() 
		window.Yes.OnLButtonUp = function() 
			ApplyPanel() lnConfig.SetHorizontal()
		end
		window:Resize(wx, wy + 45)
	end
    window.SetHorizontal = e
	
    e = window("Button")
    e:Resize(275)
    e:SetText(L"Scroll Defaults")
    e:AnchorTo(window.SetHorizontal, "bottom", "top", 0, 10)
    e.OnLButtonUp = function() 
		window.ApplyLabel:Show() window.Yes:Show() window.No:Show() 
		window.Yes.OnLButtonUp = function() 
			ApplyPanel() lnConfig.SetScroll()
		end
		window:Resize(wx, wy + 45)
	end
    window.SetScroll = e
	
    e = window("Button")
    e:Resize(275)
    e:SetText(L"Event Text Default")
    e:AnchorTo(window.SetScroll, "bottom", "top", 0, 10)
    e.OnLButtonUp = function() 
		window.ApplyLabel:Show() window.Yes:Show() window.No:Show() 
		window.Yes.OnLButtonUp = function() 
			ApplyPanel() lnConfig.SetEvents()
		end
		window:Resize(wx, wy + 45)
	end
    window.SetEvents = e
	
    e = window("Button")
    e:Resize(275)
    e:SetText(L"Event Align Centered")
    e:AnchorTo(window.SetEvents, "bottom", "top", 0, 10)
    e.OnLButtonUp = function() 
		window.ApplyLabel:Show() window.Yes:Show() window.No:Show() 
		window.Yes.OnLButtonUp = function() 
			ApplyPanel() lnConfig.SetAligns1()
		end
		window:Resize(wx, wy + 45)
	end
    window.SetAlign1 = e
	
    e = window("Button")
    e:Resize(275)
    e:SetText(L"Event Align Justified")
    e:AnchorTo(window.SetAlign1, "bottom", "top", 0, 10)
    e.OnLButtonUp = function() 
		window.ApplyLabel:Show() window.Yes:Show() window.No:Show() 
		window.Yes.OnLButtonUp = function() 
			ApplyPanel() lnConfig.SetAligns2()
		end
		window:Resize(wx, wy + 45)
	end
    window.SetAlign2 = e
	
	
    e = window("Label")
    e:Resize(120)
    e:Align("rightcenter")
    e:AnchorTo(window.SetAlign2, "bottomleft", "topleft", 0, 10)
    e:SetText(L"Are you sure?")
	e:Font("font_clear_small_bold")
    window.ApplyLabel = e
	
    e = window("Button")
    e:Resize(70)
    e:SetText(L"Yes")
    e:AnchorTo(window.ApplyLabel, "right", "left", 8, 0)
    window.Yes = e
	
    e = window("Button")
    e:Resize(70)
    e:SetText(L"No")
    e:AnchorTo(window.Yes, "right", "left", 7, 0)
    e.OnLButtonUp = function() ApplyPanel() end
    window.No = e
    
    return window
end

function ApplyPanel()
	window.ApplyLabel:Hide() window.Yes:Hide() window.No:Hide() window:Resize(wx, wy)
end

function lnConfig.UpdateTo2()
	local FullOptions = {}
	FullOptions["HIT"] = {250, 30, 50, L"", L"", 3, 0, "left", "leftcenter"}
	FullOptions["HIT_CRIT"] = {250, 30, 50, L"*", L"*", 4, 0, "left", "leftcenter"}
	FullOptions["HITOUT"] = {43, 110, 255, L"",L"", 3, 0, "right", "rightcenter"}
	FullOptions["HITOUT_CRIT"] = {43, 110, 255, L"*",L"*", 4, 0, "right", "rightcenter"}
	FullOptions["SPELL"] = {250, 30, 50, L"", L"", 3, 0, "left", "leftcenter"}
	FullOptions["SPELL_CRIT"] = {250, 30, 50, L"*", L"*", 4, 0, "left", "leftcenter"}
	FullOptions["SPELLOUT"] = {43, 110, 255, L"",L"", 3, 0, "right", "rightcenter"}
	FullOptions["SPELLOUT_CRIT"] = {43, 110, 255, L"*",L"*", 4, 0, "right", "rightcenter"}
	FullOptions["HEAL"] = {23, 255, 75, L"", L"", 3, 0, "right", "rightcenter"}
	FullOptions["HEAL_CRIT"] = {23, 255, 75, L"*", L"*", 4, 0, "right", "rightcenter"}
	FullOptions["HEALOUT"] = {23, 255, 75, L"", L"", 3, 0, "right", "rightcenter"}
	FullOptions["HEALOUT_CRIT"] = {23, 255, 75, L"*", L"*", 4, 0, "right", "rightcenter"}
	FullOptions["ABILITY"] = {90, 150, 210, L"", L"", 4, "New", "right", "rightcenter"}
	FullOptions["DEATH"] = {135, 77, 199, L"", L"", 4, "Death", "right", "rightcenter"}
	FullOptions["DISRUPT"] = {130, 130, 188, L"", L"", 3, "Disr.", "right", "rightcenter"}
	FullOptions["BLOCK"] = {130, 130, 188, L"", L"", 3, "Block", "right", "rightcenter"}
	FullOptions["EVADE"] = {130, 130, 188, L"", L"", 3, "Dodge", "right", "rightcenter"}
	FullOptions["PARRY"] = {130, 130, 188, L"", L"", 3, "Parry", "right", "rightcenter"}
	FullOptions["ABSORB"] = {130, 130, 188, L"", L"", 3, "Abs.", "right", "rightcenter"}
	FullOptions["DISRUPTOUT"] = {210, 80, 80, L"", L"", 4, "Disr.", "left", "leftcenter"}
	FullOptions["BLOCKOUT"] = {210, 80, 80, L"", L"", 4, "Block", "left", "leftcenter"}
	FullOptions["EVADEOUT"] = {210, 80, 80, L"", L"", 4, "Dodge", "left", "leftcenter"}
	FullOptions["PARRYOUT"] = {210, 80, 80, L"", L"", 4, "Parry", "left", "leftcenter"}
	FullOptions["ABSORBOUT"] = {210, 80, 80, L"", L"", 4, "Abs.", "left", "leftcenter"}
	FullOptions["XP"] = {126, 222, 126, L"+", L"", 4, 20, "right", "rightcenter"}
	FullOptions["RP"] = {124, 30, 220, L"+", L"", 4, 20, "right", "rightcenter"}
	FullOptions["INF"] = {0, 111, 252, L"+", L"", 4, 5, "right", "rightcenter"}
	
	for key, value in pairs(FullOptions) do
		if NaturalLog.LogOptions[key] == nil then
			NaturalLog.LogOptions[key] = FullOptions[key]
		else
			for event, values in pairs(value) do
				if NaturalLog.LogOptions[key][event] == nil then
					NaturalLog.LogOptions[key][event] = FullOptions[key][event]
				end
			end
		end
	end
	
	if NaturalLog.BasicVars.ArraySideways == false then
		NaturalLog.Assign( "spacing", 3 )
	else
		NaturalLog.Assign( "spacing", 1 )
		lnConfig.SetAligns1()
	end
	
	lnConfig.SetScroll()
	NaturalLog.Update()
end

function lnConfig.SetVertical()
	NaturalLog.Assign( "direction", true )
	NaturalLog.Assign( "orient", false )
	NaturalLog.Assign( "Alpha", .8 )
	NaturalLog.Assign( "LogLimit", 400 )
	NaturalLog.Assign( "size", .07, .78 )
	NaturalLog.Assign( "lines", 30 )
	NaturalLog.Assign( "spacing", 3 )
	NaturalLog.Update()
end
function lnConfig.SetHorizontal()
	NaturalLog.Assign( "direction", false )
	NaturalLog.Assign( "orient", true )
	NaturalLog.Assign( "Alpha", .8 )
	NaturalLog.Assign( "LogLimit", 400 )
	NaturalLog.Assign( "size", .85, .025 )
	NaturalLog.Assign( "lines", 15 )
	NaturalLog.Assign( "spacing", 1 )
	NaturalLog.Update()
end
function lnConfig.SetScroll()
	NaturalLog.Assign( "activecolor", 159, 254, 100, .66 )
	NaturalLog.Assign( "inactivecolor", 62, 158, 255, .45 )
	NaturalLog.Assign( "ScrollSize", 16 )
	NaturalLog.Assign( "Scroll", { true, false, true, true, false, true } )
	NaturalLog.Update()
end
function lnConfig.SetEvents()
	NaturalLog.LogOptions["HIT"] = {250, 30, 50, L"", L"", 3, 0, "left", "leftcenter"}
	NaturalLog.LogOptions["HIT_CRIT"] = {250, 30, 50, L"*", L"*", 4, 0, "left", "leftcenter"}
	NaturalLog.LogOptions["HITOUT"] = {43, 110, 255, L"",L"", 3, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["HITOUT_CRIT"] = {43, 110, 255, L"*",L"*", 4, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["SPELL"] = {250, 30, 50, L"", L"", 3, 0, "left", "leftcenter"}
	NaturalLog.LogOptions["SPELL_CRIT"] = {250, 30, 50, L"*", L"*", 4, 0, "left", "leftcenter"}
	NaturalLog.LogOptions["SPELLOUT"] = {43, 110, 255, L"",L"", 3, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["SPELLOUT_CRIT"] = {43, 110, 255, L"*",L"*", 4, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["HEAL"] = {23, 255, 75, L"", L"", 3, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["HEAL_CRIT"] = {23, 255, 75, L"*", L"*", 4, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["HEALOUT"] = {23, 255, 75, L"", L"", 3, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["HEALOUT_CRIT"] = {23, 255, 75, L"*", L"*", 4, 0, "right", "rightcenter"}
	NaturalLog.LogOptions["ABILITY"] = {90, 150, 210, L"", L"", 4, "New", "right", "rightcenter"}
	NaturalLog.LogOptions["DEATH"] = {135, 77, 199, L"", L"", 4, "Death", "right", "rightcenter"}
	NaturalLog.LogOptions["DISRUPT"] = {130, 130, 188, L"", L"", 3, "Disr.", "right", "rightcenter"}
	NaturalLog.LogOptions["BLOCK"] = {130, 130, 188, L"", L"", 3, "Block", "right", "rightcenter"}
	NaturalLog.LogOptions["EVADE"] = {130, 130, 188, L"", L"", 3, "Dodge", "right", "rightcenter"}
	NaturalLog.LogOptions["PARRY"] = {130, 130, 188, L"", L"", 3, "Parry", "right", "rightcenter"}
	NaturalLog.LogOptions["ABSORB"] = {130, 130, 188, L"", L"", 3, "Abs.", "right", "rightcenter"}
	NaturalLog.LogOptions["DISRUPTOUT"] = {210, 80, 80, L"", L"", 4, "Disr.", "left", "leftcenter"}
	NaturalLog.LogOptions["BLOCKOUT"] = {210, 80, 80, L"", L"", 4, "Block", "left", "leftcenter"}
	NaturalLog.LogOptions["EVADEOUT"] = {210, 80, 80, L"", L"", 4, "Dodge", "left", "leftcenter"}
	NaturalLog.LogOptions["PARRYOUT"] = {210, 80, 80, L"", L"", 4, "Parry", "left", "leftcenter"}
	NaturalLog.LogOptions["ABSORBOUT"] = {210, 80, 80, L"", L"", 4, "Abs.", "left", "leftcenter"}
	NaturalLog.LogOptions["XP"] = {126, 222, 126, L"+", L"", 4, 20, "right", "rightcenter"}
	NaturalLog.LogOptions["RP"] = {124, 30, 220, L"+", L"", 4, 20, "right", "rightcenter"}
	NaturalLog.LogOptions["INF"] = {0, 111, 252, L"+", L"", 4, 5, "right", "rightcenter"}
	NaturalLog.Update()
end
function lnConfig.SetAligns1()
	NaturalLog.LogOptions["HIT"][8] = "left" NaturalLog.LogOptions["HIT"][9] = "center"
	NaturalLog.LogOptions["HIT_CRIT"][8] = "left" NaturalLog.LogOptions["HIT_CRIT"][9] =  "center"
	NaturalLog.LogOptions["HITOUT"][8] = "right" NaturalLog.LogOptions["HITOUT"][9] = "center"
	NaturalLog.LogOptions["HITOUT_CRIT"][8] = "right" NaturalLog.LogOptions["HITOUT_CRIT"][9] = "center"
	NaturalLog.LogOptions["SPELL"][8] = "left" NaturalLog.LogOptions["SPELL"][9] = "center"
	NaturalLog.LogOptions["SPELL_CRIT"][8] = "left" NaturalLog.LogOptions["SPELL_CRIT"][9] = "center"
	NaturalLog.LogOptions["SPELLOUT"][8] = "right" NaturalLog.LogOptions["SPELLOUT"][9] = "center"
	NaturalLog.LogOptions["SPELLOUT_CRIT"][8] = "right" NaturalLog.LogOptions["SPELLOUT_CRIT"][9] = "center"
	NaturalLog.LogOptions["HEAL"][8] = "right" NaturalLog.LogOptions["HEAL"][9] = "center"
	NaturalLog.LogOptions["HEAL_CRIT"][8] = "right" NaturalLog.LogOptions["HEAL_CRIT"][9] = "center"
	NaturalLog.LogOptions["HEALOUT"][8] = "right" NaturalLog.LogOptions["HEALOUT"][9] = "center"
	NaturalLog.LogOptions["HEALOUT_CRIT"][8] = "right" NaturalLog.LogOptions["HEALOUT_CRIT"][9] = "center"
	NaturalLog.LogOptions["ABILITY"][8] = "right" NaturalLog.LogOptions["ABILITY"][9] = "center"
	NaturalLog.LogOptions["DEATH"][8] = "right" NaturalLog.LogOptions["DEATH"][9] = "center"
	NaturalLog.LogOptions["DISRUPT"][8] = "right" NaturalLog.LogOptions["DISRUPT"][9] = "center"
	NaturalLog.LogOptions["BLOCK"][8] = "right" NaturalLog.LogOptions["BLOCK"][9] = "center"
	NaturalLog.LogOptions["EVADE"][8] = "right" NaturalLog.LogOptions["EVADE"][9] = "center"
	NaturalLog.LogOptions["PARRY"][8] = "right" NaturalLog.LogOptions["PARRY"][9] = "center"
	NaturalLog.LogOptions["ABSORB"][8] = "right" NaturalLog.LogOptions["ABSORB"][9] = "center"
	NaturalLog.LogOptions["DISRUPTOUT"][8] = "left" NaturalLog.LogOptions["DISRUPTOUT"][9] = "center"
	NaturalLog.LogOptions["BLOCKOUT"][8] = "left" NaturalLog.LogOptions["BLOCKOUT"][9] = "center"
	NaturalLog.LogOptions["EVADEOUT"][8] = "left" NaturalLog.LogOptions["EVADEOUT"][9] = "center"
	NaturalLog.LogOptions["PARRYOUT"][8] = "left" NaturalLog.LogOptions["PARRYOUT"][9] = "center"
	NaturalLog.LogOptions["ABSORBOUT"][8] = "left" NaturalLog.LogOptions["ABSORBOUT"][9] = "center"
	NaturalLog.LogOptions["XP"][8] = "right" NaturalLog.LogOptions["XP"][9] = "center"
	NaturalLog.LogOptions["RP"][8] = "right" NaturalLog.LogOptions["RP"][9] = "center"
	NaturalLog.LogOptions["INF"][8] = "right" NaturalLog.LogOptions["INF"][9] = "center"
	NaturalLog.Update()
end
function lnConfig.SetAligns2()
	NaturalLog.LogOptions["HIT"][8] = "left" NaturalLog.LogOptions["HIT"][9] = "leftcenter"
	NaturalLog.LogOptions["HIT_CRIT"][8] = "left" NaturalLog.LogOptions["HIT_CRIT"][9] =  "leftcenter"
	NaturalLog.LogOptions["HITOUT"][8] = "right" NaturalLog.LogOptions["HITOUT"][9] = "rightcenter"
	NaturalLog.LogOptions["HITOUT_CRIT"][8] = "right" NaturalLog.LogOptions["HITOUT_CRIT"][9] = "rightcenter"
	NaturalLog.LogOptions["SPELL"][8] = "left" NaturalLog.LogOptions["SPELL"][9] = "leftcenter"
	NaturalLog.LogOptions["SPELL_CRIT"][8] = "left" NaturalLog.LogOptions["SPELL_CRIT"][9] = "leftcenter"
	NaturalLog.LogOptions["SPELLOUT"][8] = "right" NaturalLog.LogOptions["SPELLOUT"][9] = "rightcenter"
	NaturalLog.LogOptions["SPELLOUT_CRIT"][8] = "right" NaturalLog.LogOptions["SPELLOUT_CRIT"][9] = "rightcenter"
	NaturalLog.LogOptions["HEAL"][8] = "right" NaturalLog.LogOptions["HEAL"][9] = "rightcenter"
	NaturalLog.LogOptions["HEAL_CRIT"][8] = "right" NaturalLog.LogOptions["HEAL_CRIT"][9] = "rightcenter"
	NaturalLog.LogOptions["HEALOUT"][8] = "right" NaturalLog.LogOptions["HEALOUT"][9] = "rightcenter"
	NaturalLog.LogOptions["HEALOUT_CRIT"][8] = "right" NaturalLog.LogOptions["HEALOUT_CRIT"][9] = "rightcenter"
	NaturalLog.LogOptions["ABILITY"][8] = "right" NaturalLog.LogOptions["ABILITY"][9] = "rightcenter"
	NaturalLog.LogOptions["DEATH"][8] = "right" NaturalLog.LogOptions["DEATH"][9] = "rightcenter"
	NaturalLog.LogOptions["DISRUPT"][8] = "right" NaturalLog.LogOptions["DISRUPT"][9] = "rightcenter"
	NaturalLog.LogOptions["BLOCK"][8] = "right" NaturalLog.LogOptions["BLOCK"][9] = "rightcenter"
	NaturalLog.LogOptions["EVADE"][8] = "right" NaturalLog.LogOptions["EVADE"][9] = "rightcenter"
	NaturalLog.LogOptions["PARRY"][8] = "right" NaturalLog.LogOptions["PARRY"][9] = "rightcenter"
	NaturalLog.LogOptions["ABSORB"][8] = "right" NaturalLog.LogOptions["ABSORB"][9] = "rightcenter"
	NaturalLog.LogOptions["DISRUPTOUT"][8] = "left" NaturalLog.LogOptions["DISRUPTOUT"][9] = "leftcenter"
	NaturalLog.LogOptions["BLOCKOUT"][8] = "left" NaturalLog.LogOptions["BLOCKOUT"][9] = "leftcenter"
	NaturalLog.LogOptions["EVADEOUT"][8] = "left" NaturalLog.LogOptions["EVADEOUT"][9] = "leftcenter"
	NaturalLog.LogOptions["PARRYOUT"][8] = "left" NaturalLog.LogOptions["PARRYOUT"][9] = "leftcenter"
	NaturalLog.LogOptions["ABSORBOUT"][8] = "left" NaturalLog.LogOptions["ABSORBOUT"][9] = "leftcenter"
	NaturalLog.LogOptions["XP"][8] = "right" NaturalLog.LogOptions["XP"][9] = "rightcenter"
	NaturalLog.LogOptions["RP"][8] = "right" NaturalLog.LogOptions["RP"][9] = "rightcenter"
	NaturalLog.LogOptions["INF"][8] = "right" NaturalLog.LogOptions["INF"][9] = "rightcenter"
	NaturalLog.Update()
end

-- Actually add the panel
panel.create = CreatePanel
panel.revert = ApplyPanel
panel.id = lnConfig.AddPanel(panel)
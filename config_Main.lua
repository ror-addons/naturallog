-------------------------------------
--  Natural Log Config  --  version: 1.0.1
--  
--  Primary page for natural log configuration
--
--     Written By NigelTufnel ( Adam.Dew@gmail.com )
-------------------------------------


local LibGUI = LibStub("LibGUI")

lnConfig = {}
lnConfig.Panels = {}

local MainWindow
local PanelCache = {}
local firstLoad = true

function lnConfig.Init()
    RegisterEventHandler(SystemData.Events.LOADING_END, "lnConfig.OnLoad")
    RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "lnConfig.OnLoad")
	if NaturalLog.BasicVars == nil or NaturalLog.LogOptions == nil then
		NaturalLog.Set("spacing", 3)
		NaturalLog.LogOptions = {}
		lnConfig.SetVertical()
		lnConfig.SetScroll()
		lnConfig.SetEvents()
		lnConfig.Show()
	elseif NaturalLog.LogOptions["HIT"][8] == nil then
		lnConfig.UpdateTo2()
		NaturalLog.LoadSettings()
		lnConfig.Show()
	else
		NaturalLog.LoadSettings()
	end
end

function lnConfig.OnLoad()
    
    if not firstLoad then return end
    firstLoad = false
    
    if LibSlash then
        if not LibSlash.IsSlashCmdRegistered("naturallog") then
            LibSlash.RegisterSlashCmd("naturallog", lnConfig.SlashHandler)
            LibSlash.RegisterSlashCmd("ln", lnConfig.SlashHandler)
			
			-- for those that miss the joke
            LibSlash.RegisterSlashCmd("nl", lnConfig.SlashHandler)
        end
    end
end

function lnConfig.SlashHandler()
	lnConfig.Show()
end

function lnConfig.TogglePanel(panel)
    local Panels = lnConfig.Panels
    
    -- If we've never loaded this panel before, create its window
    if not PanelCache[panel] then
        PanelCache[panel] = Panels[panel]:create()
        WindowClearAnchors(PanelCache[panel].name)
        WindowAddAnchor(PanelCache[panel].name, "left", MainWindow.name, "right", 5, 0)
        WindowSetPopable(PanelCache[panel].name, false)
    end
    
    if lnConfig.ActivePanel then
        PanelCache[lnConfig.ActivePanel]:Hide()
		Panels[lnConfig.ActivePanel]:revert()
        if lnConfig.ActivePanel == panel then
            lnConfig.ActivePanel = nil
            return
        end
    end
    
    lnConfig.ActivePanel = panel
    Panels[panel]:revert()
    PanelCache[panel]:Show()
end


function lnConfig.AddPanel(panel)
    table.insert(lnConfig.Panels, panel)
    return #lnConfig.Panels
end

function lnConfig.Show(panel)
    -- We wait until the main menu is first shown to actually construct it
    if not MainWindow then
        lnConfig.BuildMainMenu()
    end
	
	NaturalLog.Assign("movable", true)
    
    MainWindow:Show()
    if panel then
        for k,p in ipairs(lnConfig.Panels) do
            if p.title == panel or k == panel then
                lnConfig.TogglePanel(k)
            end
        end
    end
end

function lnConfig.BuildMainMenu()
    if MainWindow and MainWindow.name and DoesWindowExist(MainWindow.name) then
        d("Attempt to create second instance of main menu ignored.")
        return
    end
    
    local Panels = lnConfig.Panels
    
    MainWindow = LibGUI("Blackframe")
    MainWindow:MakeMovable()
    MainWindow:Resize(300, 300)
    MainWindow:AnchorTo("Root", "right", "right", -200, -100)
    
    MainWindow.Title = MainWindow("Label")
    MainWindow.Title:Resize(250)
    MainWindow.Title:AnchorTo(MainWindow, "top", "top", 0, 10)
    MainWindow.Title:Font("font_clear_large_bold")
    MainWindow.Title:SetText(L"Natural Log")
    MainWindow.Title:Align("center")
    MainWindow.Title:IgnoreInput()
    
    MainWindow.Container = MainWindow("Window")
    MainWindow.Container:Resize(230, (#Panels + 1) * 45)
    MainWindow.Container:AnchorTo(MainWindow.Title, "bottom", "top", 0, 10)
    
    -- Set up panel buttons
    MainWindow.Container.Buttons = {}
    for k,p in ipairs(Panels) do
        local B = MainWindow.Container("Button")
        B:Resize(230)
        B:SetText(p.title)
        B.OnLButtonUp = function() lnConfig.TogglePanel(k) end
        if k == 1 then
            B:AnchorTo(MainWindow.Container, "topleft", "topleft", 0, 2)
        else
            B:AnchorTo(MainWindow.Container.Buttons[k-1], "bottomleft", "topleft", 0, 4)
        end
        MainWindow.Container.Buttons[k] = B
    end
    
    -- Finally, make the main window's height actually properly fit the container, whatever size it is.
    MainWindow:Resize(300, MainWindow.Container.height+20)
    
    -- Add a close button
    MainWindow.CloseButton = MainWindow("Closebutton")
    MainWindow.CloseButton.OnLButtonUp =
        function()
            if lnConfig.ActivePanel then
                PanelCache[lnConfig.ActivePanel]:Hide()
				Panels[lnConfig.ActivePanel]:revert()
            end
            lnConfig.ActivePanel = nil
            MainWindow:Hide()
			NaturalLog.Assign("movable", false)
        end
    
end 
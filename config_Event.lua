-------------------------------------
--  Natural Log Config  --  version: 1.0.1
--  
--  Config panel for event entries
--
--     Written By NigelTufnel ( Adam.Dew@gmail.com )
-------------------------------------

if not lnConfig then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel, RevertPanel
local panel = {}
local active = { ["event"] = 0, ["color"] = {0,0,0}, ["text"]="123", ["font"] = 0 }
lnConfig.events = {
	{"HIT", "Physical Dmg"},
	{"HIT_CRIT", "Physical Dmg Crit"},
	{"HITOUT", "Physical Dmg Dealt"},
	{"HITOUT_CRIT", "Physical Dmg Dealt Crit"},
	{"SPELL", "Ability Dmg"},
	{"SPELL_CRIT", "Ability Dmg Crit"},
	{"SPELLOUT", "Ability Dmg Dealt"},
	{"SPELLOUT_CRIT", "Ability Dmg Dealt Crit"},
	{"HEAL", "Heal Recieved"},
	{"HEAL_CRIT", "Heal Recieved Crit"},
	{"HEALOUT", "Heal Dealt"},
	{"HEALOUT_CRIT", "Heal Dealt Crit"},
	{"ABILITY", "Ability Granted"},
	{"DEATH", "Player Deaths"},
	{"DISRUPT", "Player Disrupted"},
	{"BLOCK", "Player Blocked"},
	{"EVADE", "Player Dodged"},
	{"PARRY", "Player Parried"},
	{"ABSORB", "Player Absorb"},
	{"DISRUPTOUT", "Target Disrupted"},
	{"BLOCKOUT", "Target Blocked"},
	{"EVADEOUT", "Target Dodged"},
	{"PARRYOUT", "Target Parried"},
	{"ABSORBOUT", "Target Absorb"},
	{"XP", "Experience Point Gain"},
	{"RP", "Renown Point Gain"},
	{"INF", "Influence Point Gain"},
}

local fonts = {
    {"font_clear_tiny", L"Myriad Pro - Tiny"},
    {"font_clear_small_bold", L"Myriad Pro - Small Bold"},
    {"font_clear_small", L"Myriad Pro - Small"},
    {"font_clear_medium_bold",  L"Myriad Pro - Medium Bold"},
    {"font_clear_medium", L"Myriad Pro - Medium"},
    {"font_clear_large_bold", L"Myriad Pro - Large Bold"},
    {"font_clear_large", L"Myriad Pro - Large"},
	{"font_default_text_small", L"Age of Reckoning - Small"},
	{"font_default_text", L"Age of Reckoning - Medium"},
	{"font_default_text_large", L"Age of Reckoning - Large"},
	{"font_default_text_huge", L"Age of Reckoning - Huge"},
}

-- Our panel window
local window
local settingfont = "font_clear_small_bold"

panel.title = L"Event Text"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if window and window.name then return window end
    
    window = LibGUI("Blackframe")
    window:Resize(420, 490)
    
    local e
    
    -- Title label
    e = window("Label")
    e:Resize(480)
    e:AnchorTo(window, "top", "top", 0, 8)
    e:Font("font_clear_large_bold")
    e:SetText(L"Event Text Settings")
    window.Title = e
	
    -- fill event combo
    e = window("Combobox")
    e:AnchorTo(window, "top", "top", 0, 40)
	for x=1,#lnConfig.events do e:Add(lnConfig.events[x][2]) end
	e:SelectIndex(1)
    window.EventBox = e
    
    -- Title label
    e = window("Label")
    e:Resize(480)
    e:AnchorTo(window, "top", "top", 0, 78)
    e:Font("font_clear_large_bold")
	e:Color(0,255,0)
    e:SetText(L"*123*")
    window.PreviewLabel = e
	
    -- Color label
    e = window("Label")
    e:Resize(90)
    e:AnchorTo(window, "topleft", "topleft", 25, 113)
    e:Font("font_clear_large_bold")
	e:Color(255,255,255)
    e:SetText(L"Color")
    e:Align("bottomleft")
    window.ColorLabel = e
	
	
	
	-- RED
    e = window("Slider")
    e:AnchorTo(window.ColorLabel, "right", "left", 25, 4)
    e:SetRange(0,255)
    window.SliderRed = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderRed, "left", "right", 0, 0)
    e:SetText("Red")
	e:Font(settingfont)
    window.LabelRed = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderRed, "right", "left", 10, 0)
    e:SetText(255)
    window.EditRed = e
	
	-- GREEN
    e = window("Slider")
    e:AnchorTo(window.SliderRed, "bottom", "top", 0, 0)
    e:SetRange(0,255)
    window.SliderGreen = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderGreen, "left", "right", 0, 0)
    e:SetText("Green")
	e:Font(settingfont)
    window.LabelGreen = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderGreen, "right", "left", 10, 0)
    e:SetText(255)
    window.EditGreen = e
	
	-- BLUE
    e = window("Slider")
    e:AnchorTo(window.SliderGreen, "bottom", "top", 0, 0)
    e:SetRange(0,255)
    window.SliderBlue = e
    
    e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.SliderBlue, "left", "right", 0, 0)
    e:SetText("Blue")
	e:Font(settingfont)
    window.LabelBlue = e
    
    e = window("Textbox")
    e:Resize(50)
    e:AnchorTo(window.SliderBlue, "right", "left", 10, 0)
    e:SetText(255)
    window.EditBlue = e
	
    -- Flags label
    e = window("Label")
    e:Resize(90)
    e:AnchorTo(window, "topleft", "topleft", 23, 225)
    e:Font("font_clear_large_bold")
	e:Color(255,255,255)
    e:SetText(L"Flags")
    e:Align("bottomleft")
    window.FlagsLabel = e
	
	--Flags
    e = window("Textbox")
    e:Resize(35)
    e:AnchorTo(window.FlagsLabel, "right", "left", 57, 4)
    window.LeadingBox = e
	
    e = window("Label")
    e:Resize(70)
    e:Align("rightcenter")
    e:AnchorTo(window.LeadingBox, "left", "right", -10, 0)
    e:SetText(L"Leading")
	e:Font(settingfont)
    window.LeadingLabel = e
	
	
    e = window("Textbox")
    e:Resize(35)
    e:AnchorTo(window.LeadingBox, "right", "left", 100, 0)
    window.TrailingBox = e
	
    e = window("Label")
    e:Resize(70)
    e:Align("rightcenter")
    e:AnchorTo(window.TrailingBox, "left", "right", -10, 0)
    e:SetText("Trailing")
	e:Font(settingfont)
    window.TrailingLabel = e
	
	
    -- Text label
    e = window("Label")
    e:Resize(60)
    e:AnchorTo(window, "topleft", "topleft", 25, 265)
    e:Font("font_clear_large_bold")
	e:Color(255,255,255)
    e:SetText(L"Text")
    e:Align("bottomleft")
    window.TextLabel = e
	
    e = window("Label")
    e:Resize(155)
    e:Align("leftcenter")
    e:AnchorTo(window.TextLabel, "right", "left", 10, 7)
    e:SetText(L"Filter If Below")
	e:Font(settingfont)
    window.FilterLabel = e
	
    e = window("Textbox")
    e:Resize(90)
	e:AnchorTo(window.FilterLabel, "right", "left", 0, -4)
    window.FilterBox = e
	
    e = window("Combobox")
    e:AnchorTo(window.TextLabel, "bottomleft", "topleft", 65, 9)
	for x=1,#fonts do e:Add(fonts[x][2]) end
	e:SelectIndex(1)
    window.FontBox = e
	
	
	
    -- Align label
    e = window("Label")
    e:Resize(60)
    e:AnchorTo(window, "topleft", "topleft", 25, 340)
    e:Font("font_clear_large_bold")
	e:Color(255,255,255)
    e:SetText(L"Aligns")
    e:Align("bottomleft")
    window.AlignLabel = e
	
	
	e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.AlignLabel, "right", "left", 0, 0)
    e:SetText("Icon Align")
	e:Font(settingfont)
    window.IconLabel = e
	
    e = window("smallCombobox")
    e:AnchorTo(window.IconLabel, "right", "left", 5, 0)
	e:Add(L"Left")
	e:Add(L"Right")
	e:SelectIndex(1)
    window.IconBox = e
	
	e = window("Label")
    e:Resize(100)
    e:Align("rightcenter")
    e:AnchorTo(window.IconLabel, "bottom", "top", 0, 0)
    e:SetText("Text Align")
	e:Font(settingfont)
    window.TextLabel = e
	
    e = window("smallCombobox")
    e:AnchorTo(window.TextLabel, "right", "left", 5, 0)
	e:Add(L"Left")
	e:Add(L"Center")
	e:Add(L"Right")
	e:SelectIndex(1)
    window.TextBox = e
	
	
    -- Apply button
    e = window("Button")
    e:Resize(175)
    e:SetText(L"Apply")
    e:AnchorTo(window, "bottomleft", "bottomleft", 25, -43)
    e.OnLButtonUp = function() ApplyPanel() end
    window.ButtonApply = e
    
    -- Revert button
    e = window("Button")
    e:Resize(175)
    e:SetText(L"Undo")
    e:AnchorTo(window, "bottomright", "bottomright", -25, -43)
    e.OnLButtonUp = function() RevertPanel() end
    window.ButtonRevert = e
    
	
    e = window("Combobox")
    e:AnchorTo(window, "bottom", "bottom", 74, -12)
	for x=1,#lnConfig.events do e:Add(lnConfig.events[x][2]) end
	e:SelectIndex(1)
    window.CopyBox = e
	
    e = window("Label")
    e:Resize(150)
    e:Align("rightcenter")
    e:AnchorTo(window.CopyBox, "left", "right", -3, 0)
    e:SetText(L"Copy Settings To")
	e:Font("font_clear_small_bold")
    window.CopyLabel = e
	
    return window
end

function RevertPanel()
	active.event = 0
	lnConfig.EventUpdate()
end

function ApplyPanel()
	local Set = NaturalLog.LogOptions[lnConfig.events[active.event][1]]
	
	Set[1] = active.color[1]
	Set[2] = active.color[2]
	Set[3] = active.color[3]
	Set[4] = window.LeadingBox:GetText()
	Set[5] = window.TrailingBox:GetText()
	Set[6] = window.FontBox:SelectedIndex()
	if type(NaturalLog.LogOptions[lnConfig.events[active.event][1]][7]) == "string" then
		Set[7] = WStringToString(window.FilterBox:GetText())
	else
		Set[7] = tonumber(window.FilterBox:GetText())
		if Set[7] == nil or Set[7] < 0 then Set[7] = 0 end
	end
	
	Set[8] = WStringToString( window.IconBox:Selected():lower() )
	Set[9] = WStringToString( window.TextBox:Selected():lower() )
	if Set[9] ~= "center" then Set[9] = Set[9].."center" end
	
	NaturalLog.Update()
end

function UpdatePanel(self, key, value)
end

function lnConfig.EventUpdate()
    if lnConfig.ActivePanel ~= panel.id then return end
	
    local sRed, sGreen, sBlue
    local eRed, eGreen, eBlue
    local updated = false
    local eupdated = false
	local tupdated = false
	local fupdated = false
	
	if active.event ~= 0 and active.event ~= window.CopyBox:SelectedIndex() then
		active.event = window.CopyBox:SelectedIndex()
		window.FilterBox:SetText(towstring(NaturalLog.LogOptions[lnConfig.events[window.CopyBox:SelectedIndex()][1]][7]))
		window.EventBox:SelectIndex(window.CopyBox:SelectedIndex())
		ApplyPanel()
	end
	
	if active.event ~= window.EventBox:SelectedIndex() then
		active.event = window.EventBox:SelectedIndex()
		
		local nRed, nGreen, nBlue, nLead, nTrail, nFont, nFilter, nIalign, nTalign = unpack(NaturalLog.LogOptions[lnConfig.events[active.event][1]])
		
        window.SliderRed:SetValue(nRed)
        window.SliderGreen:SetValue(nGreen)
        window.SliderBlue:SetValue(nBlue)
		
        window.EditRed:SetText(towstring(nRed))
        window.EditGreen:SetText(towstring(nGreen))
        window.EditBlue:SetText(towstring(nBlue))
		
		window.LeadingBox:SetText(towstring(nLead))
		window.TrailingBox:SetText(towstring(nTrail))
		
		window.FontBox:SelectIndex(nFont) 
		
		window.FilterBox:SetText(towstring(nFilter))
		if type(nFilter) == "string" then
			window.FilterLabel:SetText(L"Display Text")
		else
			window.FilterLabel:SetText(L"Filter If Below")
		end
		
		if nIalign == "left" then window.IconBox:SelectIndex(1) else window.IconBox:SelectIndex(2) end
		if nTalign == "leftcenter" then window.TextBox:SelectIndex(1) elseif nTalign == "center" then window.TextBox:SelectIndex(2) else window.TextBox:SelectIndex(3) end
		
		window.CopyBox:SelectIndex(window.EventBox:SelectedIndex())
		
		updated = true
	end
	
    -- Check to see if color selectors have been updated
    sRed = math.floor(window.SliderRed:GetValue())
    sGreen = math.floor(window.SliderGreen:GetValue())
    sBlue = math.floor(window.SliderBlue:GetValue())
        
    eRed = tonumber(window.EditRed:GetText()) or sRed
    eGreen = tonumber(window.EditGreen:GetText()) or sGreen
    eBlue = tonumber(window.EditBlue:GetText()) or sBlue
    
    if sRed ~= active.color[1] then
        active.color[1] = sRed
        updated = true
    elseif eRed ~= active.color[1] then
        active.color[1] = eRed
        eupdated = true
    end
    
    if sGreen ~= active.color[2] then
        active.color[2] = sGreen
        updated = true
    elseif eGreen ~= active.color[2] then
        active.color[2] = eGreen
        eupdated = true
    end
    
    if sBlue ~= active.color[3] then
        active.color[3] = sBlue
        updated = true
    elseif eBlue ~= active.color[3] then
        active.color[3] = eBlue
        eupdated = true
    end
	
	local text
	
	if type(NaturalLog.LogOptions[lnConfig.events[active.event][1]][7]) == "string" then
		text = window.LeadingBox:GetText()..towstring(NaturalLog.LogOptions[lnConfig.events[active.event][1]][7])..window.TrailingBox:GetText()
	else
		text = window.LeadingBox:GetText()..L"123"..window.TrailingBox:GetText()
	end
	
	tupdated = text ~= active.text
	local font = window.FontBox:SelectedIndex()
	fupdated = font ~= active.font
	
    if updated or eupdated then
	    window.SliderRed:SetValue(active.color[1])
	    window.SliderGreen:SetValue(active.color[2])
	    window.SliderBlue:SetValue(active.color[3])
	    
	    if not eupdated then
	        window.EditRed:SetText(towstring(active.color[1]))
	        window.EditGreen:SetText(towstring(active.color[2]))
	        window.EditBlue:SetText(towstring(active.color[3]))
	    end
	    
	    window.PreviewLabel:Color(active.color[1], active.color[2], active.color[3])
	end
	if tupdated then
		active.text = text
		window.PreviewLabel:SetText(active.text)
	end
	if fupdated then
		active.font = font
		window.PreviewLabel:Font(fonts[active.font][1])
	end
end

-- Actually add the panel
panel.create = CreatePanel
panel.revert = RevertPanel
panel.id = lnConfig.AddPanel(panel)
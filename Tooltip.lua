if not lnTooltip then lnTooltip = {} end

lnTooltip = Frame:Subclass("lnTooltip")

function lnTooltip:New(window)
	local t = setmetatable({}, self)
	
	t.window = window
	CreateWindowFromTemplate( t.window, "LN_TOOLTIP", "Root" )
	WindowClearAnchors( t.window )
	WindowAddAnchor( t.window, "topleft", "CursorWindow", "bottom", 0, -10 )
	WindowSetAlpha( t.window, .65 )
	t:Hide()
	
	return( t )
end 

function lnTooltip:Hide()
	WindowSetShowing(self.window, false)
end 
function lnTooltip:SetText(entry)
	if not entry then return end

	WindowSetDimensions( self.window, 900, 30 )
	WindowSetDimensions( self.window.."Value", 900, 30 )
	
	local entryLen = 0
	local entryLog = towstring(entry)
	
	repeat
		LabelSetText( self.window.."Value", wstring.sub( entryLog, 0, 64 ))
		a,b = LabelGetTextDimensions( self.window.."Value" )
		entryLen = entryLen + select( 1, a )
		entryLog = wstring.sub( entryLog, 65 )
	until wstring.len(entryLog) < 1
	
	WindowSetShowing( self.window.."Value", false )
	WindowSetDimensions( self.window, entryLen + 12, 30 )
	WindowSetDimensions( self.window.."Text", entryLen + 12, 30 )
	TextEditBoxSetText( self.window.."Text", towstring(entry) )
	WindowSetShowing(self.window, true)
end 
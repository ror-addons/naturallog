<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    
  <UiMod name="NaturalLog" version="1.0" date="10/19/2008">        
    <Author name="Adam" email="adam.dew@gmail.com" />            
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />          
    <Description text="The natural combat log" />        
    <Dependencies />        
    <Files>            
      <File name="Logger.lua" />            
      <File name="Entry.lua" />            
      <File name="Bar.lua" />            
      <File name="Handler.lua" />            
      <File name="Tooltip.lua" />            
      <File name="NaturalLog.lua" />            
      <File name="NaturalLog.xml" />            
      <File name="LibStub.lua" />            
      <File name="LibGUI.lua" />            
      <File name="config_Main.lua" />            
      <File name="config_General.lua" />            
      <File name="config_Scroll.lua" />            
      <File name="config_Event.lua" />            
      <File name="config_Default.lua" />        
    </Files>        
    <OnInitialize>            
      <CallFunction name="NaturalLog.OnLoad" />        
    </OnInitialize>        
    <OnUpdate>            
      <CallFunction name="lnConfig.EventUpdate" />            
      <CallFunction name="lnConfig.ScrollUpdate" />            
      <CallFunction name="lnConfig.GeneralUpdate" />		
    </OnUpdate>        
    <OnShutdown />		
    <SavedVariables>			
      <SavedVariable name="NaturalLog.BasicVars"/>			
      <SavedVariable name="NaturalLog.LogOptions"/>		
    </SavedVariables>    
  </UiMod>
</ModuleFile>